import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { HelperService } from "./helper.service";

const BACKEND_URL = environment.apiUrl;

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private helper: HelperService
  ) {}

  login(email: string, password: string) {
    const authData = { email, password };
    this.http
      .post<{
        token: string;
        userId: string;
        expiresIn: number;
      }>(BACKEND_URL + "login", authData)
      .subscribe(
        (response) => {
          console.log(response);
          if (response.token) {
            localStorage.setItem("token", response.token);
            localStorage.setItem("userId", response.userId);
            alert("logged in sucessfully");
          }
        },
        (error) => {
          console.log("something went wrong");
        }
      );
  }

  addUser(value) {
    return this.http.post(BACKEND_URL + "create-user", value);
  }
}
