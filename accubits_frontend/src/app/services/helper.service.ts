import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
  ) { }

  b64EncodeUnicode(str: any) {
    return btoa(
      encodeURIComponent(str).replace(
        /%([0-9A-F]{2})/g,
        // tslint:disable-next-line:only-arrow-functions
        function (match, p1) {
          return String.fromCharCode(parseInt(p1, 16));
        }
      )
    );
  }

  notification(type: any, message: any) {
    alert(message)
  }
}
