import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HelperService } from '../services/helper.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(private helper: HelperService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('token');

    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    }

    console.log(request);
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401) {
            console.error(`HTTP status code: 401 : Not Authorized Request requires user authentication`);
          } else if (error.status === 404) {
            console.error(`HTTP status code: 404 : Document or file requested by the client was not found.`);
          } else if (error.status === 405) {
            console.error(`HTTP status code: 405 : Method Not Allowed Method specified
             in the Request-Line was not allowed for the specified resource.`);
          } else if (error.status === 501) {
            console.error(`HTTP status code: 501 :  Not Implemented Request unsuccessful as the server
             could not support the functionality needed to fulfill the request.`);
          } else {
            console.error(`Backend returned HTTP status code: ${error.status}, ` + `: ${error.error.message}`);
          }
        }
        let errorMessage = 'An unknown error occurred!';
        if (error.error.message) {
          errorMessage = error.error.message;
        }
        this.helper.notification('error', errorMessage);
        return throwError(error);
      })
    );
  }
}
