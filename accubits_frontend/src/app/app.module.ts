import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { SignComponent } from "./sign/sign.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HelperService } from "./services/helper.service";
import { AuthService } from "./services/auth.service";
import { LoaderInterceptorService } from "./interceptors/loader-interceptor.service";
import { HttpConfigInterceptor } from "./interceptors/httpconfig.interceptor";
import { LoaderService } from "./services/loader.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [AppComponent, LoginComponent, SignComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true,
    },
    AuthService,
    HelperService,
    LoaderService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
