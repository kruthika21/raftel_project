import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HelperService } from "../services/helper.service";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean;

  constructor(
    private fb: FormBuilder,
    private helperService: HelperService,
    private authService: AuthService
  ) {}
  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]],
    });
  }

  get fetch() {
    return this.loginForm.controls;
  }

  onLogin() {
    this.submitted = true;
    console.log(this.loginForm.invalid);
    if (this.loginForm.invalid) {
      return;
    }
    const email =
      this.helperService.b64EncodeUnicode(this.loginForm.value.email) +
      "$" +
      this.token(40);
    const password =
      this.helperService.b64EncodeUnicode(this.loginForm.value.password) +
      "$" +
      this.token(40);
    this.authService.login(email, password);
  }

  rand = () => Math.random().toString(36).substr(2);
  token = (length: any) =>
    (
      this.rand() +
      this.rand() +
      this.rand() +
      this.rand() +
      this.rand() +
      this.rand()
    ).substr(0, length);
}
