import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HelperService } from "../services/helper.service";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-sign",
  templateUrl: "./sign.component.html",
  styleUrls: ["./sign.component.scss"],
})
export class SignComponent implements OnInit {
  signupForm: FormGroup;
  submitted: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}
  ngOnInit() {
    this.signupForm = this.fb.group({
      name: ["",[Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]],
    });
  }

  get fetch() {
    return this.signupForm.controls;
  }

  onSignup() {
    this.submitted = true;
    if (this.signupForm.invalid) {
      return;
    }
    this.authService.addUser(this.signupForm.value).subscribe((data) => {
      this.router.navigate([""]);
    }),
      (error) => {
        return error;
      };
  }
}
