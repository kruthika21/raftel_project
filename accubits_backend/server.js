const express = require("express");
const cors = require("cors");
require("dotenv").config();
const logger = require("./common/logger");
const rateLimit = require("express-rate-limit");
const app = express();
var http = require("http").Server(app);
const { NODE_ENV, PORT } = process.env;
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;



if (NODE_ENV === "production") {
    app.use(
        require("morgan")("combined", {
            stream: logger.stream
        })
    );
} else {
    app.use(
        require("morgan")("dev", {
            stream: logger.stream
        })
    );
}
app.set("trust proxy", true);
const limit = rateLimit({
    max: 100, // max requests
    windowMs: 60 * 60 * 1000, // 1 Hour of 'ban' / lockout
    message: "Too many requests" // message to send
});
app.use(cors());
app.use(express.json());
require("dotenv").config();
require("./db-connection/db");
require("./db-connection/redis-db");
require("./db-connection/access")(app);
require("./route", limit)(app);


//defualt route
app.get("/", (req, res) => {
    res.status(200).end("Application Started!");
});

const port = PORT || 3000;
http.listen(port, () => logger.info(`Listening on port ${port}`));


module.exports = app;
