/* eslint-disable linebreak-style */
const userModel = require("../model/user-model");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const redis_controller = require("../controller/redis-controller");
const { ObjectId } = require("mongoose").Types.ObjectId;
const _ = require("lodash");





module.exports = class UserController {
    static async createUser(requestData) {
        return new Promise((resolve, reject) => {
            // Find query to check the duplication of email during sign up
            userModel
                .findOne({ "email": requestData.email })
                .then(async email => {
                    if (email) {
                        reject({ message: "This email address is already registered" });
                    } else {
                        let passwordhash = requestData.password;
                        // hashing of password before storing it the DB
                        passwordhash = await bcrypt.hashSync(passwordhash, bcrypt.genSaltSync(8));

                        const user_data = new userModel({
                            "name": requestData.name,
                            "email": requestData.email,
                            "password": passwordhash
                        });

                        // Storing the data in the user colletion
                        user_data
                            .save()
                            .then(data => {
                                resolve({ user: data }, { message: "successfully created a user" });
                            }).catch(err => {
                                reject(err);
                            });
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }


    static userLogin(requestObject) {
        // De-crypting the email and password to proceed further
        let realPassword = requestObject.password.split("$");
        let realEmail = requestObject.email.split("$");
        let password = b64DecodeUnicode(realPassword[0]);
        let email = b64DecodeUnicode(realEmail[0]);

        return new Promise((resolve, reject) => {
            // This Authenticate the user whether email exist , password exist and password is valid
            userModel.getAuthenticated(email, password, async (err, user, reason) => {
                if (err) reject(err);
                if (user) {
                    // If the Authentication is successfull its generate the token and store the token in the redis DB
                    var token = tokenGeneration(user.email, user.id);
                    resolve({ token: token, userId: user.id });
                } else {
                    var reasons = userModel.failedLogin;
                    switch (reason) {
                        case reasons.NOT_FOUND:
                            reject({
                                message: "User not found : please verify your email address!"
                            });
                            break;
                        case reasons.PASSWORD_INCORRECT:
                            reject({
                                message: "Password was invalid"
                            });
                            break;
                        case reasons.PASSWORD_NULL:
                            reject({
                                message: "Password was NULL : password filed can't be empty"
                            });
                            break;
                    }
                }
            });
        });
    }

    static getUserDetail(requestObject) {
        return new Promise(async (resolve, reject) => {
            // List all the sign up user expect the requesting user
            let userDetails = await userModel
                .find({ _id: { $ne: ObjectId(requestObject._id) } }).sort({ createdAt: -1 })
                .catch(
                    (error) => {
                        reject({ error, message: "Unable to Read" });
                    }
                );
            resolve({ userDetails, message: "sucessfully" });
        });
    }


    static getUserDetailById(requestObject) {
        return new Promise(async (resolve, reject) => {
            // List the detail of the user by Id
            let userDetails = await userModel
                .findById(requestObject.id).lean()
                .catch(
                    (error) => {
                        reject({ error, message: "Unable to Read" });
                    }
                );
            // Fetching the token from redis DB with respect to the userId
            var tokenDetails = await redis_controller.getTokenById(requestObject.id).catch((err) => {
                reject(err);
            });
            //Comparing whether the token from redis db and the userDetain by ID is not null then a token is generated and saved in the redis DB
            if (!_.isNull(userDetails) && !_.isNull(tokenDetails)) {
                userDetails['extended_token'] = tokenGeneration(userDetails.email, userDetails._id);
            }
            resolve({ userDetails, message: "sucessfully" });
        });
    }

    static getLoggedInDetail() {
        return new Promise(async (resolve, reject) => {
            let responseArr = [];
            // Fetching all the active tokens from the redis db
            var tokenDetails = await redis_controller.getToken().catch((err) => {
                reject(err);
            });

            //Fetching all related user details within the active token list fetched from the above query
            var userDetails = await userModel.find({
                _id: {
                    $in: tokenDetails.map(e => e._id)
                }
            }).lean().catch((err) => {
                reject(err);
            });
            // Comparing whether the user details and active token list array exist and as same length
            if (tokenDetails.length > 0 && userDetails.length > 0 && (userDetails.length == tokenDetails.length)) {
                //Mering the 2 array into one with the common value before sending the response
                responseArr = _.merge([], userDetails, tokenDetails);
            }
            resolve(responseArr);

        });
    }

};

function b64DecodeUnicode(str) {
    // Decoding the email and password obtion in the req
    return decodeURIComponent(
        Array.prototype.map
            .call(Buffer.from(str, "base64").toString("ascii"), function (c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
    );
}


function tokenGeneration(email, id) {
    // Generating the token
    const token = jwt.sign({
        email: email,
        _id: id
    },
        process.env.JWT_KEY, {
        expiresIn: "1h"
    }
    );
    //Storage of the token in the redis DB
    redis_controller.setToken(id.toString(), JSON.stringify({ "loggedInAt": Date.now(), "access_token": token }));
    return token;
}
