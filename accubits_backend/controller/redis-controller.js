const redis_client = require("../db-connection/redis-db");
const jwt = require("jsonwebtoken");
const { ObjectId } = require("mongoose").Types.ObjectId;




module.exports = class UserController {
    static setToken(userId, tokenValue) {
        // Query to set the key-value in redis DB
        return redis_client.set(userId, tokenValue);
    }

    static getToken() {
        return new Promise(async (resolve, reject) => {
            //Quering to fetch all the keys in the redis DB
            redis_client.keys('*', function (error, tokenDetails) {
                if (error) {
                    reject(error);
                }
                //Quering to fetch all the Value with respect to the keys in the redis DB
                redis_client.mget(tokenDetails, function (error, valueDetails) {
                    if (error) {
                        reject(error);
                    }
                    //Combinating the key and value array into one array
                    let combinedValue = valueDetails.reduce(function (result, field, index) {
                        //Parse the value storing in the redis DB since its an object
                        let parsedField = JSON.parse(field);
                        //Converting the unix date value into Date String
                        parsedField.loggedInAt = new Date(parsedField.loggedInAt).toLocaleDateString();
                        //Stored the key propertis the _id field to form the array of Object
                        parsedField['_id'] = ObjectId(tokenDetails[index]);
                        //Validating whether the stored token in the redis DB is expired ot not
                        jwt.verify(parsedField.access_token, process.env.JWT_KEY, function (err, decoded) {
                            if (decoded) {
                                result.push(parsedField);
                            }
                        });
                        return result;
                    }, [])
                    resolve(combinedValue);
                });
            });
        })
    }

    static getTokenById(userId) {
        return new Promise((resolve, reject) => {
            // Quering to fetch the value by key in redis DB
            redis_client.get(userId, function (error, tokenDetails) {
                if (error) {
                    reject(error);
                }
                resolve(tokenDetails);
            });
        })
    }
};