/* eslint-disable linebreak-style */

const userRoutes = require("../route/user-route");
const auth = require("../middleware/auth");


module.exports = app => {
  app.use("/api", auth, userRoutes);
};