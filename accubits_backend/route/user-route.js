/* eslint-disable linebreak-style */
const express = require("express");
const router = express.Router();
const userController = require("../controller/user-controller");

//Register the user
router.post("/create-user", (req, res) => {
    userController
        .createUser(req.body, req.headers.origin)
        .then(data => {
            res.status(201).send(data);
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

//Login of user
router.post('/login', (req, res) => {
    userController.userLogin(req.body).then((data) => {
        res.status(201).send(data);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

//List all the signed up user details
router.get('/getUserdetail', (req, res) => {
    userController.getUserDetail(req.userData).then((data) => {
        res.status(201).send(data);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

//List all the signed up user details
router.get('/getUserdetailById/:id', (req, res) => {
    userController.getUserDetailById(req.params).then((data) => {
        res.status(201).send(data);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

// List all the logged in user detail if the token is valid
router.get('/getLoggedInDetail', (req, res) => {
    userController.getLoggedInDetail(req).then((data) => {
        res.status(201).send(data);
    }).catch((err) => {
        res.status(500).send(err);
    });
});




module.exports = router;
