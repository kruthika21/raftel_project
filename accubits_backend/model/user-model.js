/* eslint-disable linebreak-style */
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: {
    type: String,
    required: true,
    unique: true,
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)$/, 'Please enter a valid email address']
  },
  password: {
    type: String, required: true
  },
  createdAt: { type: Date, default: Date.now }
}, { strict: true });

userSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (
    err,
    isMatch
  ) {
    if (err) return err;
    cb(null, isMatch);
  });
};

var reasons = (userSchema.statics.failedLogin = {
  NOT_FOUND: 0,
  PASSWORD_INCORRECT: 1,
  PASSWORD_NULL: 3,
});

userSchema.statics.getAuthenticated = function (email, password, cb) {
  this.findOne({
    "email": email
  }, (err, user) => {
    if (err) return cb(err);

    if (!user) {
      return cb(null, null, reasons.NOT_FOUND);
    }

    if (!user.password) {
      return cb(null, null, reasons.PASSWORD_NULL);
    }

    user.comparePassword(password, (err, isMatch) => {
      if (err) return cb(err);
      if (isMatch) {
        return cb(null, user);
      }
      return cb(null, null, reasons.PASSWORD_INCORRECT);
    });
  });
};
module.exports = mongoose.model("user_schema", userSchema, "user_schema");