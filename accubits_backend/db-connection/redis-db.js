const redis = require('redis');
const redis_client = redis.createClient();

redis_client.on('connect', function () {
    console.log('Redis client connected');

});

redis_client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});


module.exports = redis_client;
