const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    //Validating whether the requested route needed token verification or not
    if (!process.env.JWT_EXCEPTIONAL_URL.includes(req.path)) {
      const token = req.headers.authorization;
      //validating and decoding  the token
      const decodedToken = jwt.verify(token, process.env.JWT_KEY);
      //Storing the docoded value in the Object
      req.userData = {
        email: decodedToken.email,
        _id: decodedToken._id
      };
    }
    next();
  } catch (error) {
    res
      .status(401)
      .json({ message: "Access Denied: You are not authenticated!" });
  }
};
